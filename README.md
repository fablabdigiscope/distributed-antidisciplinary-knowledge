# Distributed Antidisciplinary Knowledge

###### **Distributed Antidisciplinary Knowledge** is the web page where Fablab UPSaclay keeps its contents - programs, courses, classes, workshops - that emphasize on **Antidisciplinarity**.

###### To us, **Antidisciplinarity** means that we are more than the sum of our professional expertise. We are citizens. And we think that what we do at work should stay aligned with our personal lives. For this reason, we ignore the barriers between discipline to the benefit of a more systemic and distributed design based on the commons. We are systemic commoners. We create and distribute knowledge outside of the restricted fields of disciplines. 
