# Summary

# EUGLOH | SYSTEMIC DESIGN FOR THE POST-OIL ERA

---

- [GLOBAL INTRODUCTION](0-introduction.md)

---

<!--

- [DAY 1 - JUNE 29, 2022](x)
# THE (PRE)HISTORY OF THE FOSSIL FUELS THAT MADE US
  - [Morning Session 1 (10am-1pm)](x)
    - [10am : EUGLOH Innovation Days Presentation (with univ gests)](presentation-innovation-days.md)
    - [10:15am : EUGLOH Innovation Days 2022 Program Description](program-2022-description.md)
    - [000 OUR STATEMENT](statement.md)
   - [+150 Years of OIL](150-years-oil.md)-->
  <!--   - [10:25am : A Prehistory of OIL](prehistory-of-oil.md) <!-- how oil was made -->
  <!--   - [11am : The Rise of OIL](rise-of-oil.md) <!-- the pioneers and the violence -->
  <!--   - [11:35am : The Full Human-OIL Symbiosis](human-oil-symbiosis.md) <!-- the technology/machinery that comes with oil -->
  <!--   - [12:05pm : The Deadly Pact](deadly-pact.md) <!-- the arm done by oil to the environment, CO2 increase, oil-spills -->
  <!--   - [000 The Politics of OIL](politics-of-oil.md) <!-- the wars -->
  <!--- [Afternoon Session 1 (2pm-4:30pm)](x)
    - [2pm : OIL Everywhere, about the benefits...](oil-everywhere.md)
     - [2:35pm : Alexandre Monnin and the "Redirection Écologique"](a-monnin.md)
     - [000 : Q&A Alexandre](q&a-alexandre.md)
  - [Night Session Option : There Will Be Blood](there-will-be-blood.md)     
  - [Night Session Option : There Will Be Blood](there-will-be-blood.md)     

---

- [DAY 2 - JUNE 30, 2022](x)
# FOSSIL FUELS AND HUMANS : A DEADLY PACT...
  - [Morning Session 2](x)
  - [What the IPCC's last reports tells us about Climate Change](ipcc-2022.md) <!-- https://usbeketrica.com/fr/article/les-5-solutions-du-giec-pour-attenuer-le-changement-climatique -->
  <!--- [What Happens When OIL Disappears ?](end-of-oil.md)
    - [The constraints](constraints.md)
    - [The possible alternatives](alternatives-to-oil.md)
    - [The Benefits](benefits-of-oil-scarcity.md)
  - [Afternoon Session 1 (2pm-4:30pm)](x)
    - [Imagination at Work : the Political Arts](political-arts.md)

---

- [DAY 3 - JULY 1ST, 2022](x)
# GETTING OUT OF THE OIL-RUT ? : POSSIBLE COMMUNITY-DRIVEN ALTERNATIVES TO MITIGATE CLIMATE CHANGE
  - [Morning Session 2](x)
  - [The IPCC's Recommandations](ipcc-2022.md)
    - [Quitting Fossil Fuels](quitting-fossil-fuesl.md)
    - [Renewable Energies](renewable-energies.md)
    - [Carbone Capture](carbone-capture.md)
    - [Behavioral Changes](behavioral-change.md)
    - [More Investment](more-investment.md)
  - [Community-Driven Opportunities ?](community-driven-opportunities.md)
    - [Technologies](technologies.md)
    - [Activism & Hacktivism](activism.md)
    - [Initiatives](initiatives.md)
  - [Afternoon Session 1 (2pm-4:30pm)](x)
    - [](.md)




---

- [STATEMENT](statement.md)

---
-->
- [FULL EVENT SCHEDULE](NEW_SCHEDULE.md)
<!--
---

- [VIDEOS](videos.md)
