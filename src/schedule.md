

![The Rust Logo](1-oil-guardian-pix.webp)
---

DAY 1
- 9h30-9h50 : Gathering at the Fablab / Coffee / Breakfast
- 10h : KATHERINE FREGNAC (10min INTRO EUHLOG)
- 10h10 : ROMAIN DI VOZZO (20min INTRO FABLAB UPSACLAY)
- 10h30 : ROMAIN DI VOZZO (90min The EVENT, INTRO TO OIL PREHISTORY and more)
- 12H00 : Biobreak
- 12h05 : JONAH MARRS (Annoucing Workshops)
- 12h10 : Lunch
- 13h30 : ALEXANDRE MONNIN (CONFIRMED)(90min REDIRECTION ÉCOLOGIQUE)
- 15h00 : Biobreak
- 15h15 : ROMAIN DI VOZZO (30min TRANSITIONNING BETWEEN ALEX AND JULIE ? OR ARTWORKS PRESENTATION ?)
- 15h45 : JULIE LEGAULT (CONFIRMED)
- 17h50 : Biobreak (10MIN)
- 17h00 : DAVID ISHEE (CONFIRMED)
- 18h15 : End of Session
- FREE TIME


1. -DAY ONE : COMMUNITY-BASED PROJECTS
---
| DAY ONE TIMELINE | TOPICS | GUESTS |
| - | - | - |
| 9:40 am - 10 am | Students gather online and on sites | ooo |
| 10 am - 10:15 am | Introduction to the EUGLOH INNOVATION DAYS 2022| RDV |
| 10:15 am - 10:25 am | Innovation Days Program | RDV |
| 10:25 am - 11 am | Redirection Écologique | Alexandre Monnin |
| 11 am - 1:30 pm | xxx | ooo |
| 1:30 pm - 2:30 pm | Lunch and drinks on site | - |
| 2:30 pm - 2:45 pm | Open-mic for EUGLOH students to share their experience or request for collaborations | Students |
| 2:45 pm - 4:45 pm | Local Field session or Lab sessions to What ? Who ? | ooo |
| 5 pm - 5:05 pm | Short closing session | ooo |

---
<!--
1. -DAY TWO : MANAGEMENT OF INNOVATIVE & ORGANIC GLOBAL NETWORK ?
######         10h = Students gather in their own universities, have a coffee and croissant, and connect through the web.
######  10h30-11h = Introduction to community-based labs and spaces by romain
######         11h-12h = Panel of speakers from European networks with Bio-Hackers and BioArtists moderated by romain/and/or ? (PANEL TO COME)
######         12h-12h20 = Questions to Panel (moderated by romain or a friend)
######         12h20-12h30 = Open-mic for EUGLOH students to share their experience or request for collaborations
######         12h-13h = Panel of speakers from the European network of Fablabs and community-partners moderated by romain/and/or ?  (PANEL TO COME)
######         13h-14h =Lunch and drinks on site
######         14h-15h15 = Panel of speakers from American networks of Fablabs (both US and South-America) by romain/and/or ? (PANEL TO COME)
######         15h15-15h30 = Questions to Panel (moderated by romain or a friend)
######         15h30-15h45 = Open-mic for EUGLOH students to share their experience or request for collaborations
######         15h45-16h = BioBreak (to rest)
######         16h-18h (or until the end of the night) = Hands-on Workshop on assembling and programming a DIY-Biology Device (turbidity meter, or centrifuge, or more complex devices according to students level)
######         (18H = Possible closing session of the day)
 
1. -DAY THREE : BOTTOM-UP INNOVATION FROM THE INSIDE OF UNIVERSITIES
######         10h45 = Students gather in their own universities, have a coffee and croissant, and connect through the web.
######         11h-11h45h = Morning introduction-session. Discussion on bottom-up innovation from the inside of top-down institutions with Prof Jean Bergounioux (AP-HP/UPSACLAY) and romain
######         11h45-12h = Questions to Jean and Romain on their collaborations
######         12h-12h15 = Open-mic for EUGLOH students to share their experience or request for collaborations
######         12h15-13h = Projects Demos from EUGLOH students (PhDs or Masters or younger if inspired/inspiring)
######         13h-14h = Lunch and drinks on site
######         14h-15h = Introduction to the Academany Programs (Fab Academy, Bio Academy, Fabricademy) by romain
######         15h-16h = Panel-Discussion on Globally Distributed Education Programs and Networks by romain/and/or ? (PANEL TO COME)
######         16h-16h30 = Questions to panel (moderated by romain or a friend)
######         16h30-17h = Wrap-Up Session by officials (photos and screenshots to share later) 
--
