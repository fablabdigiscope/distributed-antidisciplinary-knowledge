![](1-oil-guardian-pix.webp)


DAY 1
- 9h30-9h50 : Gathering at the Fablab / Coffee
- 10h : KATHERINE FREGNAC (10min INTRO EUHLOG)
- 10h10 : ROMAIN DI VOZZO (20min INTRO FABLAB UPSACLAY)
- 10h30 : ROMAIN DI VOZZO (90min The EVENT, INTRO TO OIL ADDICTION, etc)
- 12H00 : Biobreak
- 12h05 : JONAH MARRS (Announcing Workshops ?)
- 12h10 : Lunch
- 13h30 : ALEXANDRE MONNIN (CONFIRMED)(90min REDIRECTION ÉCOLOGIQUE)
- 15h00 : Biobreak
- 15h15 : ROMAIN DI VOZZO (30min TRANSITIONNING BETWEEN ALEX AND JULIE ? OR ARTWORKS PRESENTATION ?)
- 15h45 : JULIE LEGAULT (CONFIRMED)
- 17h50 : Biobreak (10MIN)
- 17h00 : DAVID ISHEE (CONFIRMED)
- 18h15 : End of Session
- FREE TIME

<!--
DAY 2
- 9h30-9h50 : Gathering at the Fablab / Coffee / Breakfast
- 10h : Speaker 1 ou WORKSHOP
- 11h 35: Biobreak (10MIN)
- 11h45 : Atelier Pizza + Repas
- 13h30 : JAMES AUGER (CONFIRMED)
- 15h00 : Biobreak (15MIN) / Coffee
- 15h15 : ANNA WALDMAN-BROWN (CONFIRMED)
- 16h45 : Biobreak (15MIN) / Coffee
- 17h00 : NORELLA CORONELL (CONFIRMED)
- 18h00 : End of Session
- FREE TIME

DAY 3
- 9h30-9h50 : Gathering at the Fablab / Coffee / Breakfast
- 10h : GOFFREY DORNE (?)
- 11h 35: Biobreak (10MIN)
- 11h45 : Atelier Pizza + Repas
- 13h30 : KALI TAYLOR (CONFIRMED)
- 15h00 : Biobreak (15MIN) / Coffee
- 15h15 : THOMAS LANDRAIN (CONFIRMED)
- 16h45 : Biobreak (15MIN) / Coffee
- 17h00 : WORKSHOP ?
- 18h00 : End of Session
- FREE TIME
