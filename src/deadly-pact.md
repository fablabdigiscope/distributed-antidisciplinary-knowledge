# DEADLY PACT
###### DAY 1, 12:05pm

---

|
- ```Assignment : tbd```

---
| OIL ADDICTION |
| :-: |
| <iframe width="560" height="315" src="https://www.youtube.com/embed/EOm18c5Btiw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> |
| ![OIL ADDICTION 2](oil-addiction-financial-times3.mov) |

---

#### OIL Consumption VS Happiness

| Country | [2020 OIL Consumption (barrels/day)](https://www.eia.gov/tools/faqs/faq.php?id=709&t=6) | [World Happiness Report 2020](https://en.wikipedia.org/wiki/World_Happiness_Report#:~:text=2020%20report,-The%202020%20report&text=Finland%20is%20the%20happiest%20country%20in%20the%20world%2C%20followed%20by,question%20asked%20in%20the%20poll.) |
| - | - | - |
| US | 1 | 18 |
| China | 2 | 94 |
| India | 3 | 144 |
| Japan | 4 | 62 |
| Saudi Arabia | 5 | 27 |
| Russia | 6 | 73 |
| Brazil | 7 | 32 |

#### Happiness VS OIL Consumption

| Country | [World Happiness Report 2022](https://worldhappiness.report/ed/2022/happiness-benevolence-and-trust-during-covid-19-and-beyond/#ranking-of-happiness-2019-2021) | [2022 OIL Consumption (barrels/day)](https://worldpopulationreview.com/country-rankings/oil-consumption-by-country)
| - | - | - |
| Finland | 1 | 54 |
| Denmark | 2 | 64 |
| Iceland | 3 | 134 |
| Switzerland | 4 | 53 |
| Netherlands | 5 | 21 |
| Sweden | 6 | 41 |
| Norway | 7 | 52 |
| - | - | - |
| Bhutan | Gross National Happiness | 178 |

---

#### The case of Bhutan

**GND** stands for [Gross National Happiness](https://en.wikipedia.org/wiki/Gross_National_Happiness), and this is what Bhutan uses to measure its population's well-being. The rest of the world uses the **GDP** : [Gross Domestic Product](https://en.wikipedia.org/wiki/Gross_domestic_product) as the main indicator to measure the well-being of its population.




<!--![2-distributed-network4.png]()

 Distributed and Antidisciplinary Knowledge
This web page is where [Fablab UPSaclay](https://fablabdigiscope.universite-paris-saclay.fr/) will keep the contents related to the **EUGLOH INNOVATION DAYS 2022**.-->
