# DISTRIBUTED ANTIDISCIPLINARY NETWORKS

---

|
- ```Assignment : Define a strategy where citizens could stop the use of OIL globally to mitigate climate change.```

---

![2-distributed-network4.png](2-distributed-network4.png)

Distributed and Antidisciplinary Knowledge
This web page is where [Fablab UPSaclay](https://fablabdigiscope.universite-paris-saclay.fr/) will keep the contents related to the **EUGLOH INNOVATION DAYS 2022**.

These contents are distributed from our Fablab, locally, and over the internet.

To us, **Antidisciplinarity** means that we are more than the sum of our professional expertise. We are citizens. And we think that what we do at work should stay aligned with our personal lives (not the opposite).

For this reason, we ignore the barriers between disciplines to the benefit of a more **systemic** and **distributed design** based on the **commons**.

**We are systemic commoners. We absorb, we create, we distribute knowledge outside of the restricted fields of disciplines.**

---
![3-gas-station.jpg](3-gas-station.jpg)

| About the End of Oil |
| :- |
| As an energy for human societies, OIL has been around for more than a century and a half. Increasing human activities and multiplying the force humans can produce per a factor of 8. Human don't get direct superpowers from fuel, they don't ingest it. They feed a global network of machineries, and a part of the oil production is also used to extract more oil. |
| OIL will not disappear tomorrow, but as a fossil fuel, only a determinated amount of it has been "produced" by nature along millions of years of anaerobic decomposition of dead sea-organisms. Humans cannot reproduce OIL, or  if they can/could, it would be to expensive (it doesn't make sens). The ratio between the amount of energy OIL produces and the quantity of OIL needed to produce oil might just increase (5% to extract oil and for 95% dor the rest of the economy - 1 barrel for 21 barrels in 2015). |
| Now, imagine that, **WE** - as citizens - want to apply the GIECs recommandations to the letter to prevent a global temperature increase of +1.5C degrees. We have 3 years to do it. We know that humanity is producing so much Greenhouse Gazes that it is now competing with the EARTH global ecosystem in the modification of the climate. The climate is changing because humans have released to much carbon dioxyde in the atmosphere. To tackle this threat that endangers our species and all the other ones, imagine that we want to stop using what's left of oil. Imagine that citizens all around the world manage to agree on such an idea. Would we be able to do it and how ? |

---
