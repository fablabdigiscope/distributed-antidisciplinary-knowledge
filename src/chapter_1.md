# Chapter 1

1. -DAY ONE : OIL and HUMANS : A deadly Romance
*         9h40h = Students gather in their own universities have a coffee and croissant, and connect through the web.
*         10h00-10h15 = Morning-introduction session on the EUGLOH INNOVATION DAYS by Romain and officials / partners, etc.
*         10h15-10h25 = Presentation of the Innovation Days program
*         10h25-11h = Manu Prakash’s Session on Foldscope (with intro from romain)
*         11h00-13h30 = Hands-on/Warm-Up Workshop with the Foldscope Kit for education-based and community-oriented biology empowerment (Manu and/or Jonah)
*         13h30-14h30 = Lunch and drinks on site
*         14h30-14h45 = Open-mic for EUGLOH students to share their experience or request for collaborations
*         14h45-16h45 = Local Field session or Lab sessions to use Foldscope, to document experiences and post on the Foldscope Community Platform. (Requires local support as well)
*         (17H = Possible closing session of the day)
 
1. -DAY TWO : MANAGEMENT OF INNOVATIVE & ORGANIC GLOBAL NETWORK ?
*         10h = Students gather in their own universities, have a coffee and croissant, and connect through the web.
*  10h30-11h = Introduction to community-based labs and spaces by romain
*         11h-12h = Panel of speakers from European networks with Bio-Hackers and BioArtists moderated by romain/and/or ? (PANEL TO COME)
*         12h-12h20 = Questions to Panel (moderated by romain or a friend)
*         12h20-12h30 = Open-mic for EUGLOH students to share their experience or request for collaborations
*         12h-13h = Panel of speakers from the European network of Fablabs and community-partners moderated by romain/and/or ?  (PANEL TO COME)
*         13h-14h =Lunch and drinks on site
*         14h-15h15 = Panel of speakers from American networks of Fablabs (both US and South-America) by romain/and/or ? (PANEL TO COME)
*         15h15-15h30 = Questions to Panel (moderated by romain or a friend)
*         15h30-15h45 = Open-mic for EUGLOH students to share their experience or request for collaborations
*         15h45-16h = BioBreak (to rest)
*         16h-18h (or until the end of the night) = Hands-on Workshop on assembling and programming a DIY-Biology Device (turbidity meter, or centrifuge, or more complex devices according to students level)
*         (18H = Possible closing session of the day)
 
1. -DAY THREE : BOTTOM-UP INNOVATION FROM THE INSIDE OF UNIVERSITIES
*         10h45 = Students gather in their own universities, have a coffee and croissant, and connect through the web.
*         11h-11h45h = Morning introduction-session. Discussion on bottom-up innovation from the inside of top-down institutions with Prof Jean Bergounioux (AP-HP/UPSACLAY) and romain
*         11h45-12h = Questions to Jean and Romain on their collaborations
*         12h-12h15 = Open-mic for EUGLOH students to share their experience or request for collaborations
*         12h15-13h = Projects Demos from EUGLOH students (PhDs or Masters or younger if inspired/inspiring)
*         13h-14h = Lunch and drinks on site
*         14h-15h = Introduction to the Academany Programs (Fab Academy, Bio Academy, Fabricademy) by romain
*         15h-16h = Panel-Discussion on Globally Distributed Education Programs and Networks by romain/and/or ? (PANEL TO COME)
*         16h-16h30 = Questions to panel (moderated by romain or a friend)
*         16h30-17h = Wrap-Up Session by officials (photos and screenshots to share later) 
