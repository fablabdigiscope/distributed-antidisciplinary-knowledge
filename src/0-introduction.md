# SYSTEMIC DESIGN FOR THE POST-OIL ERA

-_-

![eugloh innovation days 2022](ROM_EUGLOH_INNOVDAYS_COVER_1.png)

-_-

# GLOBAL INTRODUCTION

-_-

## About FABLABS (links)
- [https://fablabdigiscope.universite-paris-saclay.fr](https://fablabdigiscope.universite-paris-saclay.fr)
- [https://fabfoundation.org](https://fabfoundation.org)
- [https://www.fablabs.io](https://www.fablabs.io)
- [https://fabacademy.org](https://fabacademy.org)
- [https://textile-academy.org](https://textile-academy.org)
- [http://fab14.fabevent.org/#speakers](http://fab14.fabevent.org/#speakers)
- [https://fab.city](https://fab.city)

- <iframe width="560" height="315" src="https://www.youtube.com/embed/f6axuhLET3U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

-_-


| The EUGLOH Innovation Days 2022 ... |
| :- |
| - Are happening locally at FABLAB UPSaclay and broadcasted/distributed within the partners of the EUGLOH Alliance |
| |
| - Have +20 participants who have traveled to France |
| |
| - Are Gender-balanced (speakers) |
| |
| - Are participative : You can ask questions at any time |
| |
| - Are inclusive : no-discrimination (of any type) is allowed |
| |
| - Are organised by FABLAB UPSaclay as a side-project |
| |
| - Are bringing 8 speakers together on the topic of the Post-Oil Era |
| |
| - Are focusing on the Oil-Era and its many related fields |
| |
| - Opposes Modernity as a path to follow : The Moderns have divided life into fields, denying the interconnections between ecosystems and species, for organisational purposes and for profit |
| |
| - Global Warming is "the" Systemic-Issue that requires Systemic Approaches and Actions (or even inactions) |
| |
| - Are based on the idea that the transformation of oil is the prime vector of the big acceleration of the human-species |
| |
| - Whish you to understand better how the lack of access to cheap and renewable energy will affect our lives in the coming 10 years (or so) |
| |
| - Will offer you new opportunities and initiatives to join to take action |
| |
| - Will try to explain how we got here, trapped into a lethal countdown to avoid +1.5C degrees |
| |
| - Are a community event |
| |
| - Hope that you will take action for your futur |
| |

<br>
<br>
<br>
<br>

---

<br>
<br>
<br>
<br>

| Speakers |
| :- |
| Alexandre Monnin |
| Anna Waldman-Brown |
| David Ishee |
| Kali Taylor |
| James Auger |
| Julie Legault |
| Norella Coronell |
| Thomas Landrain |


<br>
<br>
<br>
<br>

---

## The Narratives of the event

| - | Type | Narratives |
| :- | - | - |
| 01 | Metaphoric | Greenhouse gases form a massive new shape of non-humans, they are at the table where politics seat, and they are going to impose a lot of pressure on human-societies. They were sequestrated, we shape-shifted them by burning them, now their new shapes modify earth's atmosphere and are threatening our species, among others |
| |
| 02 | Technical | students to fill in ?  |
| |
| 03 | Religious | students to fill in ? |
| |
| 04 | Political | stuents to fill in ? |
| |
| 05 | Scientific | students to fill in ? |
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/EOm18c5Btiw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---
## About Oil Addiction, Studying and Distributed Design

#### By Romain Di Vozzo

There is something wonderful about studying at a university. During a few years of your life, you prepare your future, you learn new things and, as much as possible, you dedicate time to it in order to somehow contribute to societies. There is no need to grow your food because you can go to the grocery-store and buy whatever you can afford to keep you fed, electricity is expensive but you can afford it for a reasonable price, water flows when you need to take a shower, and you still can buy cheap tech from Alibaba and clothes from the fast fashion industries if you’re on a short budget. You’re maybe not always at ease financially speaking but you expect your dedication to offer you a bright future.

That is because someone, somewhere, mainly people working for companies, works to design the packages of the goods you buy, to design the taste of your foods, the texture of your clothes, reaching some levels of sophistication that often implies a lot of work to keep you attracted. You don't have to grow your food by yourself, you don't have to collect seeds, you don't have to plant them and to make sure they grow : you just have to assemble industrially-made "parts" that are edible, in most cases. Your clothes and furnitures are made under standards that seem to fit your requirements. At least that’s what you are being told.

But, have you ever thought that this industrial way of producing things is one of the reasons why you still have time to study ? Even if you have to compile small jobs to reimburse your student-loans, you don't have to grow your food, you don’t have to produce the energy to recharge the batteries of your bicycle or smartphones, and you don’t have to make your clothes. And if you like cooking it is even like a leisure sometimes, when you cook for friends on a Saturday night. But none of this would be possible without the automation and mechanization of the workforce. Machines have multiplied the power of the human workforce per a factor of 8.

My point here is that we actually have the time to study because we don't have to grow our food, we don’t have to take care of the animals we eat, we don’t have to extract water ourselves from a remote well, we don’t have to build our houses, etc. And all this was made possible thanks - or because - of the energy that's available, at a cheap price.

Oil is the energy that has accelerated human-societies expansion beyond the limits of the planet they are leaving on, turning most of them into addicts. And it happened more than 150 years before you were born. It came from a tendency to centralise the production of goods in some dedicated places that are easier to manage, and easier to control. Industry is a mean of political control over the so called masses. This is Design at systemic scale.

Some Industrial-Design activities and processes are overly dominative and very aggressive towards non-humans, towards the environment, and towards other humans. As a field, as a set of tools and constraints, and as a way of thinking and living on Earth, Design has a responsability to regenerate its core and to introduce more ethics into its activities.

But now that we kind of collectively understand that oil-stocks are decreasing and that our planet’s resources are limited, now that we know that without oil, getting a master degree might very well become possible only for the wealthiest people and maybe impossible for the « rest » of the population (whatever it means), I’d like to ask you a very simple question : what are you planning on doing to counter-balance the systemic issues our generation and the next-ones will face ? And how to use Design (or what ever it will become) to mitigate these issues ?

You might have noticed that in many cases, Eco-friendliness is a façade, Green-Capitalism opportunities are fakes, Carbone Markets are Prank Calls. Let’s not leave Design to just keep being a façade as well. Help, provide, care, share, spread, reproduce designs that anticipate the scarcity of ressources and materials we might face on a planet with +4 C degrees. And you won't trap your childrens on an educational regressive path where providing for basic needs will compose the only everyday-life option.

On a systemic level, Distributed, Open-source and Open Hardware Designs might help human-societies in absorbing the shock of a global deficiency in energy, when all the oil on Planet Earth will have been released as carbone dioxyde in its atmosphere. Remember, contemporary technology is also Oil-addict.

But Design could be better than a weapon for massive-control. It could largely become a Common as it already is in a few occasions and if we do everything to share it as evenly as possible between people. We need to turn Design into something that's better distributed, between places also. We need Designers and Designs that are disconnected from the Markets-Bubbles to connect better to the Commons areas of society.

Fablabs (or their descendants), as globally distributed nodes of global networks, might contribute to the mitigation of scarcity when it hits, as we’ve seen during the pandemic. Fablabs might also be working as buffers between the consumers and industries that are largely dependent on high quantity of oil to produce massively everything we use today when they ran out of fossil-energy. When the future of such industries becomes uncertain, let’s make sure we keep some alternatives alive and ready.

The risk is high that transitioning from Oil to no oil might create some very conflictual situations between peoples, between nations and between populations. If Designers are still creative enough to disconnect from the capitalistic-bubble of the Anthropocene, and to contribute broadly to a globally distributed network of peoples and places, studying at the university might still be a possible way of life.

« Designers are thinking of the beautiful objects they make and this is the risk in Design » (James Auger).

---

**"Greenhouse gases form a massive new shape of non-humans. They are at the table where politics seat, and they are going to impose a lot of pressure on human-societies...Please understand : They were sequestrated. We shape-shifted them by burning them. Now their new shape threatens earth's atmosphere and our species, among others."**


---
![gaz station for sale](derrick.jpg)
![oil spill in the ocean](burning-iol-platform1.jpg)
![gaz station for sale](3-gas-station.jpg)
![use bicycles](1653927522431.jpeg)

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/3A-ZPJvBbaQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![brands in fashion](1654093096455.jpeg)

---
#### Oil Addiction

<iframe width="560" height="315" src="https://www.youtube.com/embed/e9wiM_mUbXU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

#### Oil & Gaz Formation 1

<iframe width="560" height="315" src="https://www.youtube.com/embed/8YHsxXEVB1M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

#### Oil & Gaz Formation 2

<iframe width="560" height="315" src="https://www.youtube.com/embed/Tudal_4x4F0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---
#### Oil Spills

<iframe width="560" height="315" src="https://www.youtube.com/embed/nshSoLw0tdI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---
#### Activism

<iframe width="560" height="315" src="https://www.youtube.com/embed/eY16RJXiTo0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---
#### Story-Telling a Catastrophy

<iframe width="560" height="315" src="https://www.youtube.com/embed/RNIpQPn8chk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---
#### Politics

<iframe width="560" height="315" src="https://www.youtube.com/embed/v-zbX-YlbMk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---
#### Simulations/Reenactment

<iframe width="560" height="315" src="https://www.youtube.com/embed/FCVCOWejlag" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


---

# DESIGN PROJECT

Interfacing with Non-Humans (?)

As a species, humans are now rivaling with geologic forces. The anthropic massive release of carbone dioxyde formerly sequestrated into earth’s undergrounds is shaping a new climate era that threatens their existence and the one of other species, at a systemic scale. The extraordinary combination between an economic dogma (capitalism) and a very concentrated source of energy (oil) has accelerated human societies, resulting in unexpected developments and destructions of an industrial type, often irreversible as they were designed negating the “finitude” of the earth’s natural ressources and the possible counter-effects of exploiting them. Disciplines like engineering and Industrial Design have demonstrated their voracious appetite for ressources to create a unmanagable set of new materialities as well as new sets of behaviours resulting into what lies under the term consumerism. Consumerism, Design and Industries have been interfacing with non-humans very badly and finally, in a very unefficient manner. The statement of the research is that the combination capitalism, oil, design and engineering can no longer be promoted nor sustained as is but rather be confronted to a - if not critical - more radical approach of interfacing with non-humans. We argue that a way to design and fabricate diminished versions of the artefacts usually designed for massive industrial production has emerged through the field fablab, proposing an alternative political path for design (but not only). Non-industrial Design processes and digital fabrication tools might play a role in the mitigation of a post-oil era where industries as we know them (oil-consuming, centralized, producing lots of wastes and contaminations) are no longer sustainable due to a lack of oil. We are sensing an opportunity for degrowth, where design is no longer industrial or a tool serving the aesthetics of the markets but rather a set of constraints deeply configured to stop entertaining the idea of a world where all ressources are infinite : a “Field” Design able to embrace scarcity, conceived to remain fully open and made to augment the Commons in a wish to minimize the markets and their destructive behaviours towards humans and non-humans. We will therefore take action to explore a fablab-made, modular, flat, flexible, parametric, foldable sensing device to interface with non-humans to question the notion of “interfacing” with non-humans itself as we suspect that the less we intend to interface, the better.

This research is about filling in the existing gap between the concept of systemic design and the design of the everyday life’s object. At both scales, design brings an important amount of interactions with inert non-humans, living non-humans, and humans. The design, the development and the fabrication of Surface.io, and its adaptation to sens greenhouse gazes at many different scales will bridge both scales.

<iframe src="https://player.vimeo.com/video/545047619?h=50ebf1bd51" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/545047619">Romain Di Vozzo - Surface-io - Fab Lab Digiscope Paris-Saclay University</a> from <a href="https://vimeo.com/fabricademy">Fabricademy, Textile Academy</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
